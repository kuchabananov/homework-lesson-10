//
//  ViewController.swift
//  HomeWork 10 lesson
//
//  Created by Евгений on 15.06.21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var progressVolume: UIProgressView!
    @IBOutlet weak var sliderVolume: UISlider!
    @IBOutlet weak var textVolume: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeLabel.text = "00:00"
        textVolume.addTarget(self, action: #selector(changeVolumeText(volume:)), for: UIControl.Event.editingChanged)
    }
    
    @IBAction func timePickerChanged(_ sender: UIDatePicker) {
    }
    
    @IBAction func alarmOff(_ sender: UISwitch) {
        clearButton.isEnabled = sender.isOn
        saveButton.isEnabled = sender.isOn
        if sender.isOn {
            timeLabel.backgroundColor = .systemBlue
        }
        else {
            timeLabel.backgroundColor = .lightGray
        }
    }
    
    @IBAction func saveTime(_ sender: UIButton) {
        let time = timePicker.date
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "by_gb")
        timeFormatter.dateFormat = "hh:mm"
        timeFormatter.timeStyle = DateFormatter.Style.short
        timeLabel.text = timeFormatter.string(from: time)
    }
    
    @IBAction func clearTime(_ sender: Any) {
        timeLabel.text = ""
    }
    
    @IBAction func slideVolume(_ sender: UISlider) {
        progressVolume.progress = sliderVolume.value
        textVolume.text = String(format: "%.1f", sliderVolume.value)
    }
    
    @objc func changeVolumeText(volume: UITextField) {
        if let value = volume.text {
            sliderVolume.setValue(Float((value)) ?? 0, animated: true)
            progressVolume.progress = Float(value) ?? 0
        }
    }

    
}

